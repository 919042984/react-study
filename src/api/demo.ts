import request from "@/utils/request";

/**
 * 第一个调用http接口
 */
export const demoAPI = () => {
  request("get", "getUserInfo").then((res) => {
    console.log(res);
    return res;
  });
};
