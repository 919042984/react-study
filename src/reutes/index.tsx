import React, { lazy } from "react";
import { Navigate } from "react-router-dom";

import Login from "../pages/login";
import NotFoount from "../pages/NotFound";

// 懒性加载
const Home = lazy(() => import("../pages/home"));

const Demo = lazy(() => import("../pages/demo"));

const withLoding = (comp: JSX.Element) => {
  return (
    // lazy()加载必须添加loading 才不会报错，防止懒性加载无法加载页面
    <React.Suspense fallback={<div> Loading...</div>}>{comp}</React.Suspense>
  );
};

const routes = [
  {
    path: "/",
    element: <Navigate to="/page1" />,
  },
  {
    path: "/login",
    element: <Login />,
  },
  {
    path: "/demo",
    element: <Demo />,
  },
  {
    path: "/",
    element: <Home />,
    children: [
      {
        path: "/page1",
        element: withLoding(<Home />),
      },
      {
        path: "/page2",
        element: withLoding(<Home />),
      },
    ],
  },
  {
    path: "*",
    element: <NotFoount />,
  },
];

export default routes;
