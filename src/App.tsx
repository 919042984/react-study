import "./App.scss";
import { BrowserRouter, useLocation, useRoutes } from "react-router-dom";
import routes from "./reutes";

// 定义一个基础的路由
const BaseOutlet = () => {
  const loca = useLocation();
  console.log(loca);


  return useRoutes(routes);
};

function App() {
  return (
    <div>
      {/* 使用浏览器缓路由 */}
      <BrowserRouter>
        {/*通过outlet占位 */}
        <BaseOutlet />
      </BrowserRouter>
    </div>
  );
}

export default App;
