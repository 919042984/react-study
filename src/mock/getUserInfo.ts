export default [
  {
    url: "test/getUserInfo",
    method: "post",
    response: () => {
      return {
        code: 200,
        message: "ok",
        data: {
          userName: "张三",
          age: 18,
        },
      };
    },
  },
];
