import { PageContainer, ProLayout } from "@ant-design/pro-components";
import { useRef, useState } from "react";
import customMenuDate from "./customMenu";

import loginImg from "@/assets/imags/login.jpg";

import Page1 from "../page1";


const waitTime = (time: number = 100) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(true);
    }, time);
  });
};

const serviceData = customMenuDate;

// eslint-disable-next-line react-refresh/only-export-components
const BasicLayout: React.FC = (props) => {
  const actionRef = useRef<{ reload: () => void }>();
  const [toggle, setToggle] = useState(false);

  return (
    <>
      <ProLayout
        title="CM"
        logo={loginImg}
        // loading={true}
        style={{ height: "100vh" }}
        actionRef={actionRef}
        suppressSiderWhenMenuEmpty={toggle}
        menu={{
          request: async () => {
            return serviceData;
          },
        }}
        location={{
          pathname: "/welcome/welcome",
        }}
      >
        <Page1/>
      </ProLayout>
    </>
  );
};
export default BasicLayout;
