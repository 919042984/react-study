// 导入样式
// import styles from "./index.module.scss";
import { mainFn } from "./Demo.ts";
import { Button } from "antd";
// import request from "@/utils/request.ts";
import { demoAPI } from "@/api/demo.ts";

const Demo = () => {
  // 测试的使用使用
  mainFn();

  function butClick(): void {
    demoAPI()
  }

  return (
    <div>
      <Button type="primary" onClick={butClick}>
        {" "}
        这个是一个按钮
      </Button>
    </div>
  );
};

export default Demo;
