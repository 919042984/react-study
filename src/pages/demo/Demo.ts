abstract class Person {
  name!: string;
  sayHello() {
    console.log(1);
  }
}

interface Rn {
  name: string;
}

// 有返回值的方法,入参类型限制了第一个为boolean
const demo = (valur: boolean, n2: number): string => {
  if (valur && n2) {
    return "正确的";
  }

  return "返回";
};

/**
 *  匿名返回值
 * @returns
 */
const demo2 = () => {
  return "demo2返回值";
};

function mainFn() {
  const _demo= demo(false, 1);
  console.log(_demo)
  const _deom2=demo2()
  console.log(_deom2)
  console.log("mian 方法");
}

class Person1 extends Person {
  name: string = "";
  sayHello(): void {
    try {
      console.log(222);
    } catch (e) {
      console.log(e);
    }

    const a: Rn = { name: "aa" };
    console.log(a);
    this.test1({ name: "23423" });
  }
  test1(a: Rn): number {
    console.log(a);
    return 1;
  }
}

export { Person, Person1, mainFn };
