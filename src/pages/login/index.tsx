import { LockOutlined, MobileOutlined, UserOutlined } from "@ant-design/icons";
import {
  LoginFormPage,
  ProConfigProvider,
  ProFormCaptcha,
  ProFormCheckbox,
  ProFormText,
} from "@ant-design/pro-components";
import { useState } from "react";
import { message, Tabs } from "antd";

import loingVideo from "@/assets/video/login.mp4";
import loginImg from "@/assets/imags/login.jpg";
import styles from "./index.module.scss";

import { useNavigate } from "react-router-dom";

type LoginType = "phone" | "account";

const Page = () => {
  const [loginType, setLoginType] = useState<LoginType>("account");
  const toNaviget = useNavigate();

  function onFinish(formData: Record<string, any>): Promise<boolean | void> {
    
    // 登录完成跳转至首页
    toNaviget("/");
    console.log(111);
  }

  return (
    <ProConfigProvider dark>
      <div className={styles.root}>
        <LoginFormPage
          onFinish={onFinish}
          logo={loginImg}
          backgroundVideoUrl={loingVideo}
          title="MK中后台"
          containerStyle={{
            backgroundColor: "rgba(0, 0, 0,0.5)",
            backdropFilter: "blur(4px)",
          }}
          subTitle="练手系统"
          actions={<div className={styles.actionsStyle}></div>}
        >
          <Tabs
            centered
            activeKey={loginType}
            onChange={(activeKey) => setLoginType(activeKey as LoginType)}
          >
            <Tabs.TabPane key={"account"} tab={"账号密码登录"} />
            {/* <Tabs.TabPane key={"phone"} tab={"手机号登录"} /> */}
          </Tabs>
          {/* {loginType === "account" && (
            <> */}
              <ProFormText
                name="username"
                fieldProps={{
                  size: "large",
                  prefix: (
                    <UserOutlined
                      style={{
                        color: styles.colorText,
                      }}
                      className={"prefixIcon"}
                    />
                  ),
                }}
                placeholder={"用户名: admin or user"}
                rules={[
                  {
                    required: true,
                    message: "请输入用户名!",
                  },
                ]}
              />
              <ProFormText.Password
                name="password"
                fieldProps={{
                  size: "large",
                  prefix: (
                    <LockOutlined
                      style={{
                        color: styles.colorText,
                      }}
                      className={"prefixIcon"}
                    />
                  ),
                }}
                placeholder={"密码: ant.design"}
                rules={[
                  {
                    required: true,
                    message: "请输入密码！",
                  },
                ]}
              />
            {/* </>
          )} */}
          {/* {loginType === "phone" && (
            <>
              <ProFormText
                fieldProps={{
                  size: "large",
                  prefix: (
                    <MobileOutlined
                      style={{
                        color: styles.colorText,
                      }}
                      className={"prefixIcon"}
                    />
                  ),
                }}
                name="mobile"
                placeholder={"手机号"}
                rules={[
                  {
                    required: true,
                    message: "请输入手机号！",
                  },
                  {
                    pattern: /^1\d{10}$/,
                    message: "手机号格式错误！",
                  },
                ]}
              />
              <ProFormCaptcha
                fieldProps={{
                  size: "large",
                  prefix: (
                    <LockOutlined
                      style={{
                        color: styles.colorText,
                      }}
                      className={"prefixIcon"}
                    />
                  ),
                }}
                captchaProps={{
                  size: "large",
                }}
                placeholder={"请输入验证码"}
                captchaTextRender={(timing, count) => {
                  if (timing) {
                    return `${count} ${"获取验证码"}`;
                  }
                  return "获取验证码";
                }}
                name="captcha"
                rules={[
                  {
                    required: true,
                    message: "请输入验证码！",
                  },
                ]}
                onGetCaptcha={async () => {
                  message.success("获取验证码成功！验证码为：1234");
                }}
              />
            </>
          )} */}

          <div className={styles.divS}>
            <ProFormCheckbox noStyle name="autoLogin">
              自动登录
            </ProFormCheckbox>
            <a className={styles.a}>忘记密码</a>
          </div>
        </LoginFormPage>
      </div>
    </ProConfigProvider>
  );
};

export default Page;
