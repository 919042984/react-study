import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
   // 作用是在样式时候使用@符号
  // 还需要运行npm i -D @types/node
  resolve: {
    alias: {
      "@": path.resolve(__dirname, './src')
    }
  }
})
