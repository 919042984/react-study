### 框架插件添加命令

```shell
# 初始化脚本
 npm init vite react-study --template react-ts

#添加 antDesing Ui组件
 yarn add antd
 #添加react 路由组件
 yarn add  react-router-dom

#添加 ant-design/pro-components  组件
 yarn add @ant-design/pro-components
#添加 ant-design/pro-components  组件样式
yarn add @emotion/css
#添加 ant-design/pro-components  icons
 yarn add @ant-design/icons
#添加axios HTTP请求框架
yarn add axios
#添加mockjs 
yarn add mockjs
yarn add vite-plugin-mock 


## 配置全局路径使用@符号
yarn add   @types/node

```

### 项目结构说明

参考目录
.
├── README.md
├── index.html 项目入口
├── mock mock 目录
├── package.json
├── public
├── src
│   ├── App.tsx 主应用
│   ├── app.module.less
│   ├── api 请求中心
│   ├── assets 资源目录（图片、less、css 等）
│   ├── components 项目组件
│   ├── constants 常量
│   └── vite-env.d.ts 全局声明
│   ├── main.tsx 主入口
│   ├── pages 页面目录
│   ├── routes 路由配置
│   ├── types ts 类型定义
│   ├── store 状态管理
│   └── utils 基础工具包
├── test 测试用例
├── tsconfig.json ts 配置
├── .eslintrc.js eslint 配置
├── .prettierrc.json prettier 配置
├── .gitignore git 忽略配置
└── vite.config.ts vite 配置

### 参考资料

https://juejin.cn/post/7079797470771249166

### 命名规范

1.包名

- 小写,多个单词通过横杆隔开: hello-word

2. 组件

- 首字母大写: TodoList.tsx

```
 // Folder Names
   src
      todo-list // Folder name
          TodoList.jsx
          TodoList.module.scss
      todo-item // Folder name
          TodoItem.jsx
```

3. 常量: 大写,下划线分割: BASE_PATH



## 配置全局路径
```

#vite.config.ts中添加配置

import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from "path"
 
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve:{
    alias:{
      "@":path.resolve(__dirname,'./src')
    }
  }
})
# tsconfig.json 添加
"compilerOptions": {
    "baseUrl": "./",
    "paths": {
      "@/*":[
        "src/*"
      ]
    }
}
